angular.module('ModalEventCtrl', ['ui.bootstrap']).
controller('ModalEventController', function($scope, $modalInstance, Reservations) {

  	$scope.checkDates = function (startsAt, endsAt) {
      if(new Date(startsAt) > new Date(endsAt)) {
 			  $scope.reservation.eventEndsAt.$setValidity("endDateError", false);
 			  return false;
      }
      else {
 			  $scope.reservation.eventEndsAt.$setValidity("endDateError", true);
 			  return true;
      }
  	};

  	$scope.addReservation = function () {
  		Reservations.create($scope.event, function(data, status) {
  			if(status) {
        	console.log('Varaus tehty');
          $modalInstance.close();
        }

        else {
          console.log('VIRHE: ' + data);
        }
  		});
  	};

  	// Sulkee muistiinpanojen modaalisen ikkunan.
  	$scope.cancel = function () {
    	$modalInstance.dismiss('cancel');
  	};
});