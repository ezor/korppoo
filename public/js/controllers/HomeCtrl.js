// public/js/controllers/HomeCtrl.js
angular.module('HomeCtrl', ['ui.bootstrap']).controller('HomeController', function($scope, $modal, Reservations) {

    $scope.reservations = [];
    $scope.old_reservations = [];


// ====================== FUNKTIOT ============================    

    $scope.findReservations = function() {
    	console.log('Haetaan varaukset');

    	Reservations.get( function(data, status) {
  			if(status) {
        		$scope.reservations = data;
        	}

        	else {
            	console.log('VIRHE: ' + data);
        	}
  		});
    };

    $scope.findOldReservations = function() {
    	console.log('Haetaan vanhat varaukset');

    	Reservations.getOldReservations( function(data, status) {
  			if(status) {
        		$scope.old_reservations = data;
        	}

        	else {
            	console.log('VIRHE: ' + data);
        	}
  		});
    };

    $scope.deleteReservation = function(reservation) {
    	Reservations.delete(reservation, function(data, status) {
    		if(status) {
        		refresh();
        	}

        	else {
            	console.log('VIRHE: ' + data);
        	}
    	});
    }

    var refresh = function() {
		$scope.findReservations();
		$scope.findOldReservations();
	};

    $scope.openEvent = function(event) {
		var modalInstance = $modal.open({
			// Määritetään ikunan html-template
			templateUrl: 'views/event.html',
			// Määritetään ikkunan kontrolleri
			controller: 'ModalEventController',

			resolve: {
				calendar_event: function() {
					refresh();
				}
			}
		});

		modalInstance.result.then(function() {
			refresh();
    	}, function () {});
	};

	refresh();
});