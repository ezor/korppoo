// public/js/appRoutes.js
    angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'HomeController'
        })

        .when('/oldresevations', {
        	templateUrl: 'views/oldreservations.html',
        	controller: 'HomeController'
        });
        
    $locationProvider.html5Mode(true);

}]);