angular.module('KorppoonVaraukset', ['ngRoute', 'appRoutes', 

	// Controllers
	'MainCtrl',
	'HomeCtrl',
	'ModalEventCtrl',

	// Services
	'ReservationsService'
	
]);