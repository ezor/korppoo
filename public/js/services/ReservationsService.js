// public/js/services/ReservationsService.js
angular.module('ReservationsService', []).factory('Reservations', ['$http', function($http) {

    return {
        
        get : function(callback) {
            return $http.get('/api/reservations/')
            .success(function(data, status){
                callback(data, true);
            })
            .error(function(data, status){
                callback(data, false);
            });
        },

        getOldReservations: function(callback) {
            return $http.get('/api/reservations/old/')
            .success(function(data, status){
                callback(data, true);
            })
            .error(function(data, status){
                callback(data, false);
            });
        },

        getReservation : function(id, callback) {
            return $http.get('/api/reservations/' + id);
        },

        create : function(reservation, callback) {
            
            // Asetetaan loppumispäivän ajaksi 23:59:59
            reservation.endsAt.setHours(23);
            reservation.endsAt.setMinutes(59);
            reservation.endsAt.setSeconds(59);

            return $http.post('/api/reservations/', reservation)
            .success(function(data, status){
                console.log('VARAUS LUOTU!');
                callback(data, true);
            })
            .error(function(data, status){
                callback(data, false);
            });
        },

        modify : function(reservationData, callback) {
            return $http.post('/api/reservations/' + id, reservationData);
        },

        delete : function(reservation, callback) {
            console.log('Poistetaan ' +reservation._id);

            return $http({
                url: '/api/reservations/' + reservation._id,
                method: 'DELETE',
                data: reservation,
                headers: {"Content-Type": "application/json"}
            })
            .success(function(data, status){
                console.log('VARAUS POISTETTU!');
                callback(data, true);
            })
            .error(function(data, status){
                callback(data, false);
            });
        }
    }       

}]);