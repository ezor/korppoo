 // app/routes.js

var mongoose = require('mongoose');
var Reservation = require('./app/models/reservation');

// db
var db = require('./config/db');

mongoose.connect(db.url); 

module.exports = {
    getreservations: function(req, res) {
        // Luodaan apumuuttuja, jolla otetaan tämä ajanhetki
        var today = new Date();

        // Haetaan tapahtumat, joiden loppuhetki on nyt tai tulevaisuudessa.
        Reservation.find().gte('endsAt', today).lean().sort({startsAt: 'asc'})
        .exec( function(err, reservations) {
            if(err) {
                console.log(err);
                res.status(400).send(err);
            }
            else {
                res.status(200).send(JSON.stringify(reservations));
            }
        });
    },

    getoldreservations: function(req, res) {
        // Luodaan apumuuttuja, jolla otetaan tämä ajanhetki
        var today = new Date();

        // Haetaan tapahtumat, joiden loppumishetki on jo mennyt.
        Reservation.find().lt('endsAt', today).lean().sort({startsAt: 'desc'})
        .exec( function(err, reservations) {
            if(err) {
                console.log(err);
                res.status(400).send(err);
            }
            else {
                res.status(200).send(JSON.stringify(reservations));
            }
        });
    },

	// Lisätään uusi varaus tietokantaan.
    addreservation: function(req, res) {
    	var new_reservation = new Reservation({
    		title: req.body.title,
    		creator: req.body.creator,
    		startsAt: req.body.startsAt,
    		endsAt: req.body.endsAt
    	});

    	new_reservation.save(function (err) {
        	if (err) { 
        		console.log(err);
        		res.status(400).send(err);
        	}
        	else
        	{
          		console.log('Varaus lisätty');
          		res.status(200).send();
        	}
      	});
    },

    deletereservation: function(req, res) {
        console.log('Poistetaan varaus ' + req.body._id);

        Reservation.findOneAndRemove({ '_id': req.body._id})
        .exec(function (err, reservation){
            if (err) { 
                console.log(err);
                res.status(400).send(err);
            }
            else
            {
                console.log('Varaus poistettu');
                res.status(200).send();
            }
        });
    }
};