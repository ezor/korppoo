// server.js

var express        = require('express');
var app            = express();
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
//var mongoose	   = require('mongoose');
    
// db
//var db = require('./config/db');

// portti
var port = process.env.PORT || 8080; 

// tietokantaan yhdistäminen
//mongoose.connect(db.url); 

app.use(bodyParser.json()); 

app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

app.use(bodyParser.urlencoded({ extended: true })); 

app.use(methodOverride('X-HTTP-Method-Override')); 

app.use(express.static(__dirname + '/public')); 

// routes ==================================================
var routes = require('./routes')

// start app ===============================================
// http://localhost:8080
app.listen(port);               
                    
console.log('Sovellus käynnissä portissa ' + port);
           
exports = module.exports = app;

// ============================================================================
// GET 

// Hakee listan varauksista
app.get('/api/reservations', routes.getreservations);
// Hakee vanhat varaukset
app.get('/api/reservations/old', routes.getoldreservations);

// ============================================================================
// POST
// Lisää varauksen
app.post('/api/reservations', routes.addreservation);

// ============================================================================
// DELETE

// Poistaa parametrina tulevan varauksen
app.delete('/api/reservations/:_id', routes.deletereservation);

// ============================================================================
// Kaikilla muilla requesteilla avataan etusivu
app.get('*', function(req, res) {
    res.sendfile('./public/index.html');
});