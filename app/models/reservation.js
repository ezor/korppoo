// app/models/event.js
var mongoose = require('mongoose');

module.exports = mongoose.model('Reservation', {
	title: {type : String, required: true},
    creator: {type : String, required: true},
    startsAt: {type : Date, required: true},
    endsAt: {type : Date, required: true}
});